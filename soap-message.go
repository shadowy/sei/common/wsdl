package wsdl

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/shadowy/go/xml"
	"reflect"
)

// Envelope - SOAP Envelope
type Envelope struct {
	XMLName xml.Name `xml:"soapenv:Envelope"`
	Soapenv string   `xml:"xmlns:soapenv,attr"`
	Tem     string   `xml:"xmlns:tem,attr"`
	Cover   *string  `xml:"xmlns:cover,attr,omitempty"`
	Header  string   `xml:"soapenv:Header"`
	Body    Body
}

// Body - SOAP Body
type Body struct {
	XMLName xml.Name `xml:"soapenv:Body"`
	Data    interface{}
	Command string
}

// UnmarshalXML - custom unmarshal xml
func (body *Body) UnmarshalXML(decoder *xml.Decoder, start xml.StartElement) (err error) {
	var result interface{}
	for {
		t, _ := decoder.Token()
		if t == nil {
			break
		}
		switch start := t.(type) {
		case xml.StartElement:
			result, err = body.parseBody(start.Name.Local, decoder, &start)
		default:
			continue
		}
		if err != nil {
			break
		}
	}
	if result != nil {
		body.Data = result
	}
	return nil
}

// MarshalXML - custom marshal xml
func (body Body) MarshalXML(encoder *xml.Encoder, start xml.StartElement) (err error) {
	err = encoder.EncodeToken(start)
	if err != nil {
		return
	}
	if body.Data != nil {
		err = encoder.EncodeElement(body.Data, xml.StartElement{Name: xml.Name{Local: body.Command}})
		if err != nil {
			return
		}
	}
	return encoder.EncodeToken(xml.EndElement{Name: start.Name})
}

func (body *Body) parseBody(name string, decoder *xml.Decoder, start *xml.StartElement) (result interface{}, err error) {
	body.Command = name
	if tp, ok := TypeRequest[name]; ok {
		result = reflect.New(tp).Elem().Addr().Interface()
	}
	err = decoder.DecodeElement(result, start)
	if err != nil {
		logrus.WithError(err).WithFields(logrus.Fields{"name": name}).Error("soap.Body.parseBody")
		return nil, err
	}
	return
}
