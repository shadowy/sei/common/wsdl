package wsdl

const (
	ErrorUnknown = iota
	ErrorSystem
	ErrorClient
	ErrorObjectIsNull
	ErrorToOrgID
	ErrorToSysID
	ErrorFromSysID
	ErrorFromOrgID
	ErrorUploadPacket
	ErrorAlreadyUploaded
	ErrorCreateSession
	ErrorAppendMessageMessageAlreadyUploaded
	ErrorUploadRestrictionLimit
	ErrorUploadPacketSize
	ErrorPacketNotFound
	ErrorPacketCannotBeDownload

	ErrorCreateSessionProcessing
	ErrorFromPositionLessZero
	ErrorCountGreatThanMax
	ErrorGreatSizePacket
	ErrorPartGreatThanSize
	ErrorValueSessionState
	ErrorRefreshSessionState
	ErrorThereAreManyRecordForRequest
	ErrorSessionState
	ErrorExecuteOperation
	ErrorSessionNotFound
	ErrorOwnerOfSession
	ErrorPacketHash
	ErrorPacketSign
	ErrorAuthorization
	ErrorBlockOrganization
)
