package wsdl

import (
	"gitlab.com/shadowy/go/xml"
	xmltype "gitlab.com/shadowy/sei/common/wsdl/xml-type"
)

type DocumentAttributesPurposeType int

const (
	DocumentAttributesPurposeType0 = 0
	DocumentAttributesPurposeType1 = 1
	DocumentAttributesPurposeType2 = 2
	DocumentAttributesPurposeType3 = 3
	DocumentAttributesPurposeType4 = 4
	DocumentAttributesPurposeType5 = 5
	DocumentAttributesPurposeType6 = 6
)

type DocumentAttributesUrgent int

const (
	DocumentAttributesUrgent0 = 0
	DocumentAttributesUrgent1 = 1
)

type TypeInn int

type EcontactEnum string

const (
	EcontactEnum0 = "р"
	EcontactEnum1 = "д"
	EcontactEnum2 = "м"
	EcontactEnum3 = "ф"
	EcontactEnum4 = "п"
	EcontactEnum5 = "і"
	EcontactEnum6 = "s"
	EcontactEnum7 = "c"
	EcontactEnum8 = "u"
)

type TypeOrgID string

type TypeIdMailBox string

type RetypeEnum string

const (
	RetypeEnum0 = "д"
	RetypeEnum1 = "з"
)

type TypeDocType int

const (
	TypeDocType0 = 0
	TypeDocType1 = 1
	TypeDocType2 = 2
)

type TypeCollection int

const (
	TypeCollection0 = 0
	TypeCollection1 = 1
)

type TypeMsgType int

const (
	TypeMsgTypeAck     = 0
	TypeMsgTypePrimary = 1
	TypeMsgType2       = 2
	TypeMsgTypeReport  = 3
	TypeMsgType4       = 4
	TypeMsgType5       = 11
	TypeMsgType6       = 12
)

type TypeMsgID string

type TypeAddresseeType int

const (
	TypeAddresseeType0 = 0
	TypeAddresseeType1 = 1
)

type TypeAddress struct {
	Body       string  `xml:",chardata" json:"body"`
	Street     *string `xml:"street,attr" json:"street"`
	House      *string `xml:"house,attr" json:"house"`
	Building   *string `xml:"building,attr" json:"building"`
	Flat       *string `xml:"flat,attr" json:"flat"`
	Settlement *string `xml:"settlement,attr" json:"settlement"`
	District   *string `xml:"district,attr" json:"district"`
	Region     *string `xml:"region,attr" json:"region"`
	Country    *string `xml:"country,attr" json:"country"`
	PostCode   *string `xml:"postcode,attr" json:"postCode"`
	Postbox    *string `xml:"postbox,attr" json:"postbox"`
	NonTypical *string `xml:"nontypical,attr" json:"nonTypical"`
}

type TypeEContact struct {
	Body string        `xml:",chardata" json:"body"`
	Type *EcontactEnum `xml:"type,attr" json:"type"`
}

type TypeOfficial struct {
	Body       string  `xml:",chardata" json:"body"`
	Department *string `xml:"department,attr" json:"department"`
	Post       *string `xml:"post,attr" json:"post"`
	Separator  *string `xml:"separator,attr" json:"separator"`
}

type TypeRankSex int

const (
	TypeRankSex0 = 0
	TypeRankSex1 = 1
	TypeRankSex2 = 2
)

type TypeRank struct {
	Body           string       `xml:",chardata" json:"body"`
	Privilege      *string      `xml:"privilege,attr" json:"privilege"`
	SocialPosition *string      `xml:"socialposition,attr" json:"socialPosition"`
	Sex            *TypeRankSex `xml:"sex,attr" json:"sex"`
}

type TypeRegNumber struct {
	Body     string       `xml:",chardata" json:"body"`
	RegDate  xmltype.Date `xml:"regdate,attr" json:"regDate"`
	RegPoint *string      `xml:"regpoint,attr" json:"regPoint"`
}

type TypeTaskNumber struct {
	Body     string       `xml:",chardata" json:"body"`
	TaskDate xmltype.Date `xml:"taskDate,attr" json:"taskDate"`
}

type TypeConfidentFlag int

const (
	TypeConfidentFlag0 = 0
	TypeConfidentFlag1 = 1
)

type TypeConfident struct {
	Body string             `xml:",chardata" json:"body"`
	Flag *TypeConfidentFlag `xml:"flag,attr" json:"flag"`
}

type TypeOrganization struct {
	FullName  *string        `xml:"fullname,attr" json:"fullName"`
	ShortName *string        `xml:"shortname,attr" json:"shortName"`
	Ownership *string        `xml:"ownership,attr" json:"ownership"`
	Ogrn      *TypeOrgID     `xml:"ogrn,attr" json:"ogrn"`
	Inn       *TypeInn       `xml:"inn,attr" json:"inn"`
	Address   *TypeAddress   `xml:"Address" json:"address"`
	EContact  []TypeEContact `xml:"Econtact" json:"eContact"`
}

type TypeOrganizationOPerson struct {
	TypeOrganization

	OfficialPerson []TypeOfficialPerson `xml:"OfficialPerson" json:"officialPerson"`
}

type TypeOrganizationOPersonSign struct {
	TypeOrganization

	OfficialPerson []TypeOfficialPersonSign `xml:"OfficialPerson" json:"officialPerson"`
}

type TypeDocNumber struct {
	Kind         string                       `xml:"kind,attr" json:"kind"`
	Organization *TypeOrganizationOPersonSign `xml:"Organization" json:"organization"`
	RegNumber    *TypeRegNumber               `xml:"RegNumber" json:"regNumber"`
}

type TypeDocTransfer struct {
	Body         string           `xml:",chardata" json:"body"`
	Os           *string          `xml:"os,attr" json:"os"`
	Type         string           `xml:"type,attr" json:"type"`
	TimeOriginal xmltype.DateTime `xml:"time_original,attr" json:"timeOriginal"`
	TypeVer      *string          `xml:"type_ver,attr" json:"typeVer"`
	CharSet      *string          `xml:"char_set,attr" json:"charSet"`
	FileName     *string          `xml:"file_name,attr" json:"fileName"`
	Description  string           `xml:"description,attr" json:"description"`
	IDNumber     string           `xml:"idnumber,attr" json:"iDNumber"`
}

type TypeName struct {
	Body        string  `xml:",chardata" json:"body"`
	SecName     *string `xml:"secname,attr" json:"secName"`
	FirstName   *string `xml:"firstname,attr" json:"firstName"`
	FathersName *string `xml:"fathersname,attr" json:"fathersName"`
}

type TypePrivatePerson struct {
	Inn      *TypeInn       `xml:"inn,attr" json:"inn"`
	PasSer   *string        `xml:"pas_ser,attr" json:"pasSer"`
	PasNum   *string        `xml:"pas_num,attr" json:"pasNum"`
	PasOrg   *string        `xml:"pas_org,attr" json:"pasOrg"`
	PasDate  *xmltype.Date  `xml:"pas_date,attr" json:"pasDate"`
	Name     *TypeName      `xml:"Name" json:"name"`
	Rank     []TypeRank     `xml:"Rank" json:"rank"`
	Address  *TypeAddress   `xml:"Address" json:"address"`
	EContact []TypeEContact `xml:"Econtact" json:"eContact"`
}

type TypePrivatePersonSign struct {
	TypePrivatePerson

	SignDate xmltype.Date `xml:"SignDate" json:"signDate"`
}

type TypeOfficialPerson struct {
	Name     *TypeName      `xml:"Name" json:"name"`
	Official []TypeOfficial `xml:"Official" json:"official"`
	Rank     []TypeRank     `xml:"Rank" json:"rank"`
	Address  *TypeAddress   `xml:"Address" json:"address"`
	EContact []TypeEContact `xml:"Econtact" json:"eContact"`
}

type TypeOfficialPersonSign struct {
	TypeOfficialPerson

	SignDate xmltype.Date `xml:"SignDate" json:"signDate"`
}

type TypeRegHistory struct {
	IDNumber     *string           `xml:"idnumber,attr" json:"iDNumber"`
	Organization *TypeOrganization `xml:"Organization" json:"organization"`
	RegNumber    *TypeRegNumber    `xml:"RegNumber" json:"regNumber"`
}

type TypeReferred struct {
	IDNumber   *string         `xml:"idnumber,attr" json:"iDNumber"`
	Retype     *RetypeEnum     `xml:"retype,attr" json:"retype"`
	RegNumber  *TypeRegNumber  `xml:"RegNumber" json:"regNumber"`
	TaskNumber *TypeTaskNumber `xml:"TaskNumber" json:"taskNumber"`
}

type TypeDocumentLink struct {
	IDNumber    string                         `xml:"idnumber,attr" json:"iDNumber"`
	Type        *TypeDocType                   `xml:"type,attr" json:"type"`
	Kind        *string                        `xml:"kind,attr" json:"kind"`
	IDNumberExt *string                        `xml:"idnumber_ext,attr" json:"iDNumberExt"`
	Pages       *int                           `xml:"pages,attr" json:"pages"`
	Title       *string                        `xml:"title,attr" json:"title"`
	Annotation  string                         `xml:"annotation,attr" json:"annotation"`
	Collection  *TypeCollection                `xml:"collection,attr" json:"collection"`
	PurposeType *DocumentAttributesPurposeType `xml:"purpose_type,attr" json:"purposeType"`
	Urgent      *DocumentAttributesUrgent      `xml:"urgent,attr" json:"urgent"`
	RegNumber   *TypeRegNumber                 `xml:"RegNumber" json:"regNumber"`
}

type OutNumber struct {
	RegNumber *TypeRegNumber `xml:"RegNumber" json:"regNumber"`
}

type TypeDocumentAuthor struct {
	OutNumber     *OutNumber                   `xml:"OutNumber" json:"outNumber"`
	Organization  *TypeOrganizationOPersonSign `xml:"Organization" json:"organization"`
	PrivatePerson *TypePrivatePersonSign       `xml:"PrivatePerson" json:"privatePerson"`
}

type TypeDocumentValidator struct {
	Attestation   string                       `xml:"attestation,attr" json:"attestation"`
	DocNumber     *TypeDocNumber               `xml:"DocNumber" json:"docNumber"`
	Organization  *TypeOrganizationOPersonSign `xml:"Organization" json:"organization"`
	PrivatePerson *TypePrivatePersonSign       `xml:"PrivatePerson" json:"privatePerson"`
}

type TypeDocumentAddressee struct {
	Type          *TypeAddresseeType       `xml:"type,attr" json:"type"`
	Referred      []TypeReferred           `xml:"Referred" json:"referred"`
	Organization  *TypeOrganizationOPerson `xml:"Organization" json:"organization"`
	PrivatePerson *TypePrivatePerson       `xml:"PrivatePerson" json:"privatePerson"`
}

type TypeDocumentWriter struct {
	Organization  *TypeOrganizationOPerson `xml:"Organization" json:"organization"`
	PrivatePerson *TypePrivatePerson       `xml:"PrivatePerson" json:"privatePerson"`
}

type TypeDocument struct {
	IDNumber    string                         `xml:"idnumber,attr" json:"iDNumber"`
	Type        *TypeDocType                   `xml:"type,attr" json:"type"`
	Kind        *string                        `xml:"kind,attr" json:"kind"`
	IDNumberExt *string                        `xml:"idnumber_ext,attr" json:"iDNumberExt"`
	Pages       *int                           `xml:"pages,attr" json:"pages"`
	Title       *string                        `xml:"title,attr" json:"title"`
	Annotation  string                         `xml:"annotation,attr" json:"annotation"`
	Collection  *TypeCollection                `xml:"collection,attr" json:"collection"`
	PurposeType *DocumentAttributesPurposeType `xml:"purpose_type,attr" json:"purposeType"`
	Urgent      *DocumentAttributesUrgent      `xml:"urgent,attr" json:"urgent"`
	RegNumber   *TypeRegNumber                 `xml:"RegNumber" json:"regNumber"`
	Confident   *TypeConfident                 `xml:"Confident" json:"confident"`
	DocNumber   *TypeDocNumber                 `xml:"DocNumber" json:"docNumber"`
	DocTransfer []TypeDocTransfer              `xml:"DocTransfer" json:"docTransfer"`
	RegHistory  []TypeRegHistory               `xml:"Reghistory" json:"regHistory"`
	Author      []TypeDocumentAuthor           `xml:"Author" json:"author"`
	Validator   []TypeDocumentValidator        `xml:"Validator" json:"validator"`
	Addressee   []TypeDocumentAddressee        `xml:"Addressee" json:"addressee"`
	Writer      *TypeDocumentWriter            `xml:"Writer" json:"writer"`
	Approval    *TypeApproval                  `xml:"Approval" json:"approval"`
}

type TaskReg int

const (
	TaskReg0 = 0
	TaskReg1 = 1
)

type TaskCopy int

const (
	TaskCopy0 = 0
	TaskCopy1 = 1
)

type ReferredControl int

const (
	ReferredControl0 = 0
	ReferredControl1 = 1
)

type Author struct {
	Organization *TypeOrganizationOPersonSign `xml:"Organization" json:"organization"`
}

type Responsible int

const (
	Responsible0 = 0
	Responsible1 = 1
	Responsible2 = 2
)

type Executor struct {
	Responsible   *Responsible             `xml:"responsible,attr" json:"responsible"`
	TaskSpecified *string                  `xml:"task_specified,attr" json:"taskSpecified"`
	Deadline      xmltype.Date             `xml:"deadline,attr" json:"deadline"`
	Organization  *TypeOrganizationOPerson `xml:"Organization" json:"organization"`
}

type TypeTaskListTask struct {
	IDNumber        string            `xml:"idnumber,attr" json:"iDNumber"`
	TaskReg         *TaskReg          `xml:"task_reg,attr" json:"taskReg"`
	TaskCopy        *TaskCopy         `xml:"task_copy,attr" json:"taskCopy"`
	Kind            *string           `xml:"kind,attr" json:"kind"`
	TaskText        string            `xml:"task_text,attr" json:"taskText"`
	Deadline        *xmltype.Date     `xml:"deadline,attr" json:"deadline"`
	ReferredControl *ReferredControl  `xml:"referred-control,attr" json:"referredControl"`
	TaskNumber      *TypeTaskNumber   `xml:"TaskNumber" json:"taskNumber"`
	Confident       *TypeConfident    `xml:"Confident" json:"confident"`
	Referred        []TypeReferred    `xml:"Referred" json:"referred"`
	Author          []Author          `xml:"Author" json:"author"`
	DocTransfer     []TypeDocTransfer `xml:"DocTransfer" json:"docTransfer"`
	Executor        []Executor        `xml:"Executor" json:"executor"`
}

type TypeTaskList struct {
	Task []TypeTaskListTask `xml:"Task" json:"task"`
}

type AddType int

const (
	AddType0 = 0
	AddType1 = 1
	AddType2 = 2
	AddType3 = 3
	AddType4 = 4
	AddType5 = 5
)

type TypeAddDocumentsFolder struct {
	AddType     *AddType           `xml:"add_type,attr" json:"addType"`
	DocTransfer []TypeDocTransfer  `xml:"DocTransfer" json:"docTransfer"`
	Note        []string           `xml:"Note" json:"note"`
	Document    []TypeDocumentLink `xml:"Document" json:"document"`
}

type TypeAddDocuments struct {
	Folder []TypeAddDocumentsFolder `xml:"Folder" json:"folder"`
}

type TypeExpansion struct {
	Organization    string               `xml:"organization,attr" json:"organization"`
	ExpVer          string               `xml:"exp_ver,attr" json:"expVer"`
	EContact        []TypeEContact       `xml:"Econtact" json:"eContact"`
	StaticExpansion *TypeStaticExpansion `xml:"StaticExpansion" json:"staticExpansion"`
}

type TypeSignInfo struct {
	DocTransferIdNumber string           `xml:"docTransfer_idnumber,attr" json:"docTransferIdNumber"`
	CertificateData     string           `xml:"CertificateData" json:"certificateData"`
	SigningTime         xmltype.DateTime `xml:"SigningTime" json:"signingTime"`
	SignData            string           `xml:"SignData" json:"signData"`
}

type TypeAcknowledgementAckType int

const (
	TypeAcknowledgementAckTypeDeliver    = 1
	TypeAcknowledgementAckTypeOpened     = 2
	TypeAcknowledgementAckTypeRegistered = 3
	TypeAcknowledgementAckTypeRejected   = 4
	TypeAcknowledgementAckType4          = 5
	TypeAcknowledgementAckType5          = 6
	TypeAcknowledgementAckType6          = 7
	TypeAcknowledgementAckType7          = 8
)

type TypeAcknowledgementAckResult struct {
	ErrorCode int    `xml:"errorcode,attr" json:"errorCode"`
	ErrorText string `xml:"errortext,attr" json:"errorText"`
}

type TypeAcknowledgement struct {
	MsgID       *TypeMsgID                     `xml:"msg_id,attr" json:"msgID"`
	AckType     *TypeAcknowledgementAckType    `xml:"ack_type,attr" json:"ackType"`
	RegNumber   *TypeRegNumber                 `xml:"RegNumber" json:"regNumber"`
	AckResult   []TypeAcknowledgementAckResult `xml:"AckResult" json:"ackResult"`
	DocTransfer *TypeDocTransfer               `xml:"DocTransfer" json:"docTransfer"`
}

type TypeHeaderBaseMsgAckNow int

const (
	TypeHeaderBaseMsgAckNow0 = 0
	TypeHeaderBaseMsgAckNow1 = 1
	TypeHeaderBaseMsgAckNow2 = 2
)

type TypeHeaderBase struct {
	Standart          string                   `xml:"standart,attr" json:"standart"`
	Version           string                   `xml:"version,attr" json:"version"`
	Charset           string                   `xml:"charset,attr" json:"charset"`
	Time              xmltype.DateTime         `xml:"time,attr" json:"time"`
	MsgType           *TypeMsgType             `xml:"msg_type,attr" json:"msgType"`
	MsgID             *TypeMsgID               `xml:"msg_id,attr" json:"msgID"`
	MsgAckNow         *TypeHeaderBaseMsgAckNow `xml:"msg_acknow,attr" json:"msgAckNow"`
	FromOrgID         *TypeOrgID               `xml:"from_org_id,attr" json:"fromOrgID"`
	FromOrganization  string                   `xml:"from_organization,attr" json:"fromOrganization"`
	FromDepartment    *TypeIdMailBox           `xml:"from_department,attr" json:"fromDepartment"`
	FromSysID         string                   `xml:"from_sys_id,attr" json:"fromSysID"`
	FromSystem        string                   `xml:"from_system,attr" json:"fromSystem"`
	FromSystemDetails *string                  `xml:"from_system_details,attr" json:"fromSystemDetails"`
	ToOrgID           *TypeOrgID               `xml:"to_org_id,attr" json:"toOrgID"`
	ToOrganization    string                   `xml:"to_organization,attr" json:"toOrganization"`
	ToDepartment      *TypeIdMailBox           `xml:"to_department,attr" json:"toDepartment"`
	ToSysID           *string                  `xml:"to_sys_id,attr" json:"toSysID"`
	ToSystem          string                   `xml:"to_system,attr" json:"toSystem"`
	ToSystemDetails   *string                  `xml:"to_system_details,attr" json:"toSystemDetails"`
}

type TypeStaticExpansion struct {
	SignInfo []TypeSignInfo `xml:"SignInfo" json:"signInfo"`
}

type TypeApproval struct {
	ApprovalRequest  *TypeApprovalRequest  `xml:"ApprovalRequest" json:"approvalRequest"`
	ApprovalResponse *TypeApprovalResponse `xml:"ApprovalResponse" json:"approvalResponse"`
}

type TypeApprovalRequest struct {
	InitiativeDocumentSender *TypeInitiativeDocumentSender `xml:"InitiativeDocumentSender" json:"initiativeDocumentSender"`
	Approver                 []TypeApprover                `xml:"Approver" json:"approver"`
}

type TypeInitiativeDocumentSender struct {
	Referred      *TypeReferred            `xml:"Referred" json:"referred"`
	Organization  *TypeOrganizationOPerson `xml:"Organization" json:"organization"`
	PrivatePerson *TypePrivatePerson       `xml:"PrivatePerson" json:"privatePerson"`
}

type TypeApprovalResponse struct {
	Attestation string  `xml:"attestation,attr" json:"attestation"`
	Comment     *string `xml:"comment,attr" json:"comment"`
}

type TypeApprover struct {
	IDNumber         string                   `xml:"idnumber,attr" json:"iDNumber"`
	Deadline         xmltype.Date             `xml:"deadline,attr" json:"deadline"`
	ReferredApprover []TypeReferredApprover   `xml:"ReferredApprover" json:"referredApprover"`
	Organization     *TypeOrganizationOPerson `xml:"Organization" json:"organization"`
	PrivatePerson    *TypePrivatePerson       `xml:"PrivatePerson" json:"privatePerson"`
}

type TypeReferredApprover struct {
	IDNumber string `xml:"idnumber,attr" json:"iDNumber"`
}

type Header struct {
	TypeHeaderBase

	XMLName         xml.Name             `xml:"Header" json:"-"`
	Document        *TypeDocument        `xml:"Document" json:"document"`
	TaskList        *TypeTaskList        `xml:"TaskList" json:"taskList"`
	AddDocuments    *TypeAddDocuments    `xml:"AddDocuments" json:"addDocuments"`
	Expansion       *TypeExpansion       `xml:"Expansion" json:"expansion"`
	Acknowledgement *TypeAcknowledgement `xml:"Acknowledgement" json:"acknowledgement"`
}
