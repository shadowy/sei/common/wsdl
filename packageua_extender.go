package wsdl

import (
	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/shadowy/go/xml"
	xmltype "gitlab.com/shadowy/sei/common/wsdl/xml-type"
	"time"
)

var textAck = []string{"", "Повідомлення доставлено без помилок", "Повідомлення було прийнято без помилок", "",
	"Документ відхилено", ""}

func (h Header) FromData(data []byte) (*Header, error) {
	var obj Header
	err := xml.Unmarshal(data, &obj)
	if err != nil {
		logrus.WithError(err).Error("Header.FromData")
		return nil, err
	}
	return &obj, nil
}

func (h *Header) ToData() ([]byte, error) {
	data, err := xml.Marshal(h)
	if err != nil {
		logrus.WithError(err).Error("Header.ToData")
		return nil, err
	}
	return data, nil
}

func (h *Header) MakeAck(ackType TypeAcknowledgementAckType, errorCode int, errorText string) *Header {
	if errorCode == 0 {
		errorText = textAck[ackType]
	}
	res := h.CreateAnswer(uuid.NewV4().String())
	res.Acknowledgement = &TypeAcknowledgement{
		MsgID:       h.MsgID,
		AckType:     &ackType,
		RegNumber:   nil,
		AckResult:   []TypeAcknowledgementAckResult{{ErrorCode: errorCode, ErrorText: errorText}},
		DocTransfer: nil,
	}
	return res
}

func (h *Header) SetHeader(msgID string) {
	id := TypeMsgID(msgID)
	h.Standart = "1207"
	h.Version = "1.5"
	h.Charset = "UTF-8"
	h.Time = xmltype.DateTime{Time: time.Now()}
	h.MsgID = &id
}

func (h *Header) AckDelivered() *Header {
	res := h.MakeAck(TypeAcknowledgementAckTypeDeliver, 0, "")
	typeMsg := TypeMsgType(TypeMsgTypeAck)
	res.MsgType = &typeMsg
	return res
}

func (h *Header) AckOpened() *Header {
	res := h.MakeAck(TypeAcknowledgementAckTypeOpened, 0, "")
	typeMsg := TypeMsgType(TypeMsgTypeAck)
	res.MsgType = &typeMsg
	return res
}

func (h *Header) AckRegistered(date time.Time, number string, point *string) *Header {
	res := h.MakeAck(TypeAcknowledgementAckTypeRegistered, 0, "")
	typeMsg := TypeMsgType(TypeMsgTypeAck)
	res.MsgType = &typeMsg
	res.Acknowledgement.RegNumber = &TypeRegNumber{
		Body:     number,
		RegDate:  xmltype.Date{Time: date},
		RegPoint: point,
	}
	return res
}

func (h *Header) AckRejected() *Header {
	res := h.MakeAck(TypeAcknowledgementAckTypeRejected, 0, "")
	typeMsg := TypeMsgType(TypeMsgTypeAck)
	res.MsgType = &typeMsg
	return res
}
