package xmltype

import (
	"gitlab.com/shadowy/go/xml"
	"time"
)

// DateTime - xml data and time
type DateTime struct {
	time.Time
}

// Now - current date time
func (c DateTime) Now() DateTime {
	return DateTime{time.Now()}
}

func (c DateTime) parse(v string) (result time.Time, err error) {
	formats := []string{"2006-01-02T15:04:05.000000", "2006-01-02T15:04:05", "2006-01-02T15:04:05Z", "2006-01-02T15:04:05.000-07:00"}
	var parse time.Time
	for i := range formats {
		format := formats[i]
		parse, err = time.Parse(format, v)
		if err == nil {
			break
		}
	}
	if err != nil {
		return
	}
	return parse, nil
}

// UnmarshalXML - set value from xml
func (c *DateTime) UnmarshalXML(d *xml.Decoder, start xml.StartElement) (err error) {

	var v string
	err = d.DecodeElement(&v, &start)
	if err != nil {
		return
	}
	res, err := c.parse(v)
	if err != nil {
		return
	}
	*c = DateTime{res}
	return nil
}

func (c *DateTime) UnmarshalXMLAttr(attr xml.Attr) error {
	res, err := c.parse(attr.Value)
	if err != nil {
		return err
	}
	*c = DateTime{res}
	return nil
}

// MarshalXML - set value to xml
func (c DateTime) MarshalXML(e *xml.Encoder, start xml.StartElement) error {
	return e.EncodeElement(c.Format("2006-01-02T15:04:05Z"), start)
}

// MarshalXMLAttr - set value to xml attribute
func (c DateTime) MarshalXMLAttr(name xml.Name) (xml.Attr, error) {
	return xml.Attr{
		Name:  name,
		Value: c.Format("2006-01-02T15:04:05Z"),
	}, nil
}
