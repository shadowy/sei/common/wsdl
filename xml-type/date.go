package xmltype

import (
	"gitlab.com/shadowy/go/xml"
	"time"
)

// Date - xml date
type Date struct {
	time.Time
}

func (c Date) parse(v string) (result time.Time, err error) {
	formats := []string{"2006-01-02", "2006-01-02-07:00"}
	var parse time.Time
	for i := range formats {
		format := formats[i]
		parse, err = time.Parse(format, v)
		if err == nil {
			break
		}
	}
	if err != nil {
		return
	}
	return parse, nil
}

// UnmarshalXML - set value from xml
func (c *Date) UnmarshalXML(d *xml.Decoder, start xml.StartElement) (err error) {
	const format = "2006-01-02"
	var v string
	err = d.DecodeElement(&v, &start)
	if err != nil {
		return
	}
	res, err := c.parse(v)
	if err != nil {
		return
	}
	*c = Date{res}
	return nil
}

func (c *Date) UnmarshalXMLAttr(attr xml.Attr) error {
	res, err := c.parse(attr.Value)
	if err != nil {
		return err
	}
	*c = Date{res}
	return nil
}

// MarshalXML - set value to xml
func (c Date) MarshalXML(e *xml.Encoder, start xml.StartElement) error {
	return e.EncodeElement(c.Format("2006-01-02"), start)
}

// MarshalXMLAttr - set value to xml attribute
func (c Date) MarshalXMLAttr(name xml.Name) (xml.Attr, error) {
	return xml.Attr{
		Name:  name,
		Value: c.Format("2006-01-02"),
	}, nil
}
