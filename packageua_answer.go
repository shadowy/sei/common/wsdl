package wsdl

import (
	"encoding/base64"
	xmltype "gitlab.com/shadowy/sei/common/wsdl/xml-type"
	"path/filepath"
	"time"
)

func (h *Header) CreateAnswer(msgID string) *Header {
	res := new(Header)
	res.SetHeader(msgID)
	res.FromOrgID = h.ToOrgID
	res.FromOrganization = h.ToSystem
	res.FromSysID = *h.ToSysID
	res.FromSystem = h.ToSystem
	res.FromSystemDetails = h.ToSystemDetails
	res.ToOrgID = h.FromOrgID
	res.ToOrganization = h.FromSystem
	res.ToSysID = &h.FromSysID
	res.ToSystem = h.FromSystem
	res.ToSystemDetails = h.FromSystemDetails
	return res
}

func (h *Header) FillDocument(document *Header) {
	var tp TypeDocType = TypeDocType0
	if document.Document == nil {
		return
	}
	h.Document = &TypeDocument{
		IDNumber:    "",
		Type:        &tp,
		PurposeType: document.Document.PurposeType,
		DocNumber:   document.Document.DocNumber,
		Writer:      document.Document.Writer,
		Addressee:   document.Document.Addressee,
		DocTransfer: []TypeDocTransfer{},
	}

	if h.Document != nil && len(h.Document.Addressee) > 0 && h.Document.DocNumber != nil {
		doc := h.Document
		to := doc.DocNumber.Organization
		from := doc.Addressee[0]
		doc.DocNumber.Organization = &TypeOrganizationOPersonSign{}
		if from.Organization != nil {
			doc.DocNumber.Organization.TypeOrganization = TypeOrganization{
				FullName:  from.Organization.FullName,
				ShortName: from.Organization.ShortName,
				Ownership: from.Organization.Ownership,
				Ogrn:      from.Organization.Ogrn,
				Inn:       from.Organization.Inn,
				Address:   from.Organization.Address,
				EContact:  from.Organization.EContact,
			}
		}
		for i := range from.Organization.OfficialPerson {
			person := TypeOfficialPersonSign{
				TypeOfficialPerson: from.Organization.OfficialPerson[i],
				SignDate:           xmltype.Date{Time: time.Now()},
			}
			doc.DocNumber.Organization.OfficialPerson = append(doc.DocNumber.Organization.OfficialPerson, person)
		}
		if to != nil {
			var tp TypeAddresseeType = TypeAddresseeType0
			doc.Addressee = []TypeDocumentAddressee{{
				Type: &tp,
				Organization: &TypeOrganizationOPerson{
					TypeOrganization: TypeOrganization{
						FullName:  to.FullName,
						ShortName: to.ShortName,
						Ownership: to.Ownership,
						Ogrn:      to.Ogrn,
						Inn:       to.Inn,
						Address:   to.Address,
						EContact:  to.EContact,
					},
					OfficialPerson: nil,
				},
			}}
			for i := range doc.Addressee {
				ref := TypeReferred{IDNumber: &document.Document.IDNumber}
				doc.Addressee[i].Referred = append(doc.Addressee[i].Referred, ref)
			}
			for i := range to.OfficialPerson {
				person := TypeOfficialPerson{
					Name:     to.OfficialPerson[i].Name,
					Official: to.OfficialPerson[i].Official,
					Rank:     to.OfficialPerson[i].Rank,
					Address:  to.OfficialPerson[i].Address,
					EContact: to.OfficialPerson[i].EContact,
				}
				doc.Addressee[0].Organization.OfficialPerson = append(doc.Addressee[0].Organization.OfficialPerson, person)
			}
		}
	}

	for i := range h.Document.Addressee {
		if len(h.Document.Addressee[i].Referred) == 0 {
			h.Document.Addressee[i].Referred = []TypeReferred{{
				IDNumber:  &h.Document.IDNumber,
				RegNumber: h.Document.RegNumber,
			}}
			continue
		}
		for k := range h.Document.Addressee[i].Referred {
			h.Document.Addressee[i].Referred[k].RegNumber = h.Document.RegNumber
		}
	}
}

func (h *Header) Answer(msgID string) *Header {
	var msgType TypeMsgType = TypeMsgTypeReport
	result := h.CreateAnswer(msgID)
	result.MsgType = &msgType
	return result
}

func (h *Header) AddFile(fileID, fileName string, data, sign []byte) *Header {
	if h.Expansion == nil {
		h.Expansion = &TypeExpansion{
			StaticExpansion: nil,
		}
	}
	if h.Expansion.StaticExpansion == nil {
		h.Expansion.StaticExpansion = &TypeStaticExpansion{
			SignInfo: []TypeSignInfo{},
		}
	}

	h.Document.DocTransfer = append(h.Document.DocTransfer, TypeDocTransfer{
		Body:        base64.StdEncoding.EncodeToString(data),
		Type:        filepath.Ext(fileName),
		Description: fileName,
		IDNumber:    fileID,
	})
	h.Expansion.StaticExpansion.SignInfo = append(h.Expansion.StaticExpansion.SignInfo, TypeSignInfo{
		DocTransferIdNumber: fileID,
		SignData:            base64.StdEncoding.EncodeToString(sign),
	})
	return h
}
