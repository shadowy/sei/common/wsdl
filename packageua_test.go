package wsdl

import (
	"gitlab.com/shadowy/go/xml"
	"io/ioutil"
	"testing"
)

const (
	fileDS1  = "./doc/examples/ДС1. До виконання.xml"
	fileDS3  = "./doc/examples/ДС3. На погодження.xml"
	fileDS5  = "./doc/examples/ДС5. Для Інформування.xml"
	fileDS6  = "./doc/examples/ДС6. Для узагальнення.xml"
	fileDS7  = "./doc/examples/ДС7. Звітність. До виконання.xml"
	fileDS9  = "./doc/examples/ДС9. Звітність. На погодження.xml"
	fileDS11 = "./doc/examples/ДС11. Звітність. Для Інформування.xml"
	fileDS12 = "./doc/examples/ДС12. Звітність. Для узагальнення.xml"
	fileDS13 = "./doc/examples/ДС13. Сповіщення про доставку.xml"
	fileDS14 = "./doc/examples/ДС14. Сповіщення про прийом.xml"
	fileDS15 = "./doc/examples/ДС15. Сповіщення про реєстрацію.xml"
	fileDS16 = "./doc/examples/ДС16. Сповіщення про відхилення.xml"
)

var examples = []string{fileDS1, fileDS3, fileDS3, fileDS5, fileDS6, fileDS7, fileDS9, fileDS11, fileDS12, fileDS13,
	fileDS14, fileDS15, fileDS16}

func TestDS_UnmarshalXML(t *testing.T) {
	for _, file := range examples {
		data, err := ioutil.ReadFile("./doc/1.xml")
		if err != nil {
			t.Errorf("File %s read error %s", file, err)
			continue
		}
		var obj Header
		err = xml.Unmarshal(data, &obj)
		if err != nil {
			t.Errorf("File %s unmarshal error %s", file, err)
			continue
		}
		ask := obj.AckDelivered()
		if ask == nil {
			t.Errorf("Ask is null")
			continue
		}
		answer := obj.CreateAnswer("1")
		answer.FillDocument(&obj)
		t.Logf("File = %s OK", file)
	}
}

/*
func TestDS_Test(t *testing.T) {
	for _, file := range []string{"./doc/1.xml"} {
		data, err := ioutil.ReadFile(file)
		if err != nil {
			t.Errorf("File %s read error %s", file, err)
			continue
		}
		var obj Header
		err = xml.Unmarshal(data, &obj)
		if err != nil {
			t.Errorf("File %s unmarshal error %s", file, err)
			continue
		}
		ask := obj.AckDelivered()
		if ask == nil {
			t.Errorf("Ask is null")
			continue
		}
		answer := obj.CreateAnswer("1")
		answer.FillDocument(&obj)
		t.Logf("File = %s OK", file)
		res, _ := xml.Marshal(answer)
		t.Log(res)
	}
}*/
