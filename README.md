
## Diagrams

```mermaid
sequenceDiagram
    participant From
    participant SEI
    participant To
    participant User
    From->>+SEI: Message WS1           
    SEI->>+To: Message WS1
    To->>-SEI: Message WS13
    SEI->>-From: Message WS13
    
    User->>+To: Open document
    To->>SEI: Message WS 14
    SEI->>From: Message WS 14
    To-->>-User: Document
    
    User->>+To: Reject
    To->>SEI: Message WS16
    SEI->>-From:  Message WS16
    
    User->>+To: Register
    To->>SEI: Message WS15
    SEI->>-From:  Message WS15
    
```
## Utils

* [xsd2go](https://gitlab.com/shadowy/go/xsd2go) - convert xsd to go
* [goxsd](https://github.com/ivarg/goxsd) - convert xsd to go
