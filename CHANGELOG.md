# CHANGELOG

<!--- next entry here -->

## 0.8.1
2021-05-25

### Fixes

- update logic for answer creation (08209abc8a6ebbb84df027c96841892d986ca568)

## 0.8.0
2020-12-01

### Features

- add reference to main document (6d3da08141a1386a664c359dcd26e47fa89a9a7e)

## 0.7.4
2020-11-30

### Fixes

- fix issue with showing file in open state (6322f88182dbd30ebad277f2de7fbe18830aadf8)

## 0.7.3
2020-11-24

### Fixes

- update answer (32bd380000d81c5cfdb4b543e92bb96948b37865)
- update answer (c3565de780f129e1b89e0372e8c7f96d4633e1e2)

## 0.7.2
2020-11-23

### Fixes

- update flow for answer (3740ceb07a731e2b9f7b208da2d9d7bab4480257)
- update flow for answer (1af0a3439d2de433a2bf6146fec1c77450efa849)

## 0.7.1
2020-10-09

### Fixes

- change format for time (cf60b8f80eae5546acbfa1a257e691c0d7e1e16b)

## 0.7.0
2020-09-30

### Features

- show additional files (606e4d6c6b5880bd3b617895ea1d3ac0f6086423)

## 0.6.0
2020-09-16

### Features

- change flow for store signs (910df81d46243d3678ca56fc309a40d0665083d6)

## 0.5.0
2020-07-26

### Features

- add method for answer (c3638c516cfac8cc2b1c2683ffd3fe254cb747ec)

## 0.4.3
2020-07-07

### Fixes

- update date parsing (a55b4495ff6ba6d6007d2ee9b2be81081364a846)

## 0.4.2
2020-07-07

### Fixes

- fix date format (72ca945f1d7d80a19e0ef0be5a98fa41a406a2ce)

## 0.4.1
2020-06-26

### Fixes

- set msg_type for ack messages (42b3f358efd152e5eb6073d54b1094f2419f492c)

## 0.4.0
2020-06-23

### Features

- marshal and unmarshal Header (593f8d63d45192f7d81ab155ffe27a1d2e045741)

## 0.3.0
2020-06-22

### Features

- add ack messages (2d484db5bc24ce887b77e0980780573697329f39)

## 0.2.1
2020-06-22

### Fixes

- add gitignore and add diagram (acebde1a5d9ba9940aa400522760676823f0e17d)

## 0.2.0
2020-06-11

### Features

- add mapping fore response (aa46090d5af6305feebcaedacbb19b37d29415ff)

## 0.1.0
2020-06-11

### Features

- initial commit (fa7d8e9f399c564b33b20c91d32217ef5a31b080)
- add utils (50a3fb41f7fd1a476fa2f65ecc639a668541cb3e)
- add structure for packageua (79c0343e29377b03f062604bd82ac14bfe529a30)

### Fixes

- update import (d9ef675272def3e8196ae3d6ff53bd16a78e01f9)