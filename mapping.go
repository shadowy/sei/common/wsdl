package wsdl

import (
	"reflect"
)

var TypeRequest = map[string]reflect.Type{
	"GetVersion":                             reflect.TypeOf(GetVersion{}),
	"DownloadMessageChunk":                   reflect.TypeOf(DownloadMessageChunk{}),
	"EndProcessingDownloadedMessage":         reflect.TypeOf(EndProcessingDownloadedMessage{}),
	"GetHistory":                             reflect.TypeOf(GetHistory{}),
	"GetInputMessages":                       reflect.TypeOf(GetInputMessages{}),
	"GetMessageValidationInfo":               reflect.TypeOf(GetMessageValidationInfo{}),
	"GetSessionInfo":                         reflect.TypeOf(GetSessionInfo{}),
	"GetSessionsInfo":                        reflect.TypeOf(GetSessionsInfo{}),
	"OpenDownloadingSession":                 reflect.TypeOf(OpenDownloadingSession{}),
	"OpenUploadingSession":                   reflect.TypeOf(OpenUploadingSession{}),
	"UploadMessageChunk":                     reflect.TypeOf(UploadMessageChunk{}),
	"GetVersionResponse":                     reflect.TypeOf(GetVersionResponse{}),
	"DownloadMessageChunkResponse":           reflect.TypeOf(DownloadMessageChunkResponse{}),
	"EndProcessingDownloadedMessageResponse": reflect.TypeOf(EndProcessingDownloadedMessageResponse{}),
	"GetHistoryResponse":                     reflect.TypeOf(GetHistoryResponse{}),
	"GetInputMessagesResponse":               reflect.TypeOf(GetInputMessagesResponse{}),
	"GetMessageValidationInfoResponse":       reflect.TypeOf(GetMessageValidationInfoResponse{}),
	"GetSessionInfoResponse":                 reflect.TypeOf(GetSessionInfoResponse{}),
	"GetSessionsInfoResponse":                reflect.TypeOf(GetSessionsInfoResponse{}),
	"OpenDownloadingSessionResponse":         reflect.TypeOf(OpenDownloadingSessionResponse{}),
	"OpenUploadingSessionResponse":           reflect.TypeOf(OpenUploadingSessionResponse{}),
	"UploadMessageChunkResponse":             reflect.TypeOf(UploadMessageChunkResponse{}),
}
