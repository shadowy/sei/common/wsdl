module gitlab.com/shadowy/sei/common/wsdl

go 1.14

require (
	github.com/fiorix/wsdl2go v1.4.7
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.6.0
	gitlab.com/shadowy/go/utils v1.0.2 // indirect
	gitlab.com/shadowy/go/xml v1.0.2
	golang.org/x/net v0.0.0-20200602114024-627f9648deb9 // indirect
)
